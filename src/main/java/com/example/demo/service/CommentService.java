package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.ReportRepository;

@Service
public class CommentService {

	@Autowired
	CommentRepository commentRepository;

	@Autowired
	ReportRepository reportRepository;

	public List<Comment> findAllComment() {
		return commentRepository.findAll();
	}

	public void saveComment(Comment comment) {
		commentRepository.save(comment);
		reportRepository.updateTimestamp(comment.getReportId());
	}

	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

	public Comment editComment(Integer id) {
		Comment comment = (Comment) commentRepository.findById(id).orElse(null);
		return comment;
	}

}