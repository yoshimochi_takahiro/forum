package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Report;
import com.example.demo.repository.CommentRepository;
import com.example.demo.repository.ReportRepository;

@Service
@Transactional
public class ReportService {

	@Autowired
	ReportRepository reportRepository;
	CommentRepository commentRepository;

	public List<Report> findAllReport() {
		return reportRepository.findAllByOrderByUpdatedDateDescIdDesc();
	}

	public List<Report> findByDateReport(String startDate, String endDate) throws ParseException {

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		if (startDate != "") {
			startDate += " 00:00:00";
		} else {
			startDate = "2022-03-01 00:00:00";
		}

		if (endDate != "") {
			endDate += " 23:59:59";
		} else {

			Calendar endNowDate = Calendar.getInstance();
			endDate = sdFormat.format(endNowDate.getTime());
		}
		Date start = sdFormat.parse(startDate);
		Date end = sdFormat.parse(endDate);

		return reportRepository.findByDate(start, end);
	}

	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	// 指定したIDの投稿を取得
	public Report showReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}
}