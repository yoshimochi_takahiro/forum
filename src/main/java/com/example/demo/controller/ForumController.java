package com.example.demo.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;

	@Autowired
	CommentService commentService;

	// 投稿内容表⽰画⾯
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		List<Report> reportData = reportService.findAllReport(); // 投稿を全件取得
		List<Comment> commentData = commentService.findAllComment();
		mav.setViewName("/top"); // 画面遷移先を指定
		mav.addObject("reports", reportData); // 投稿データオブジェクトを保管
		mav.addObject("comments", commentData);
		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		Report report = new Report(); // form用の空のentityを準備
		mav.setViewName("/new"); // 画面遷移先を指定
		mav.addObject("formModel", report); // 準備した空のentityを保管
		return mav;
	}

	// 投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		reportService.saveReport(report); // 投稿をテーブルに格納
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 投稿編集処理
	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.editReport(id); // 編集する投稿を取得
		mav.addObject("formModel", report); // 編集する投稿をセット
		mav.setViewName("/edit");
		return mav;
	}

	// 投稿の編集内容更新処理
	@PutMapping("/update/{id}")
	public ModelAndView updateContent(@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
		report.setId(id); // UrlParameterのidを更新するentityにセット
		reportService.saveReport(report); // 編集した投稿を更新
		return new ModelAndView("redirect:/");
	}

	// 投稿削除
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable Integer id) {
		reportService.deleteReport(id); // UrlParameterのidを基に投稿を削除
		return new ModelAndView("redirect:/");
	}

	// 投稿詳細
	@GetMapping("/show/{id}")
	public ModelAndView showReport(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.editReport(id); // 編集する投稿を取得
		List<Comment> commentData = commentService.findAllComment();
		mav.addObject("formModel", report); // 編集する投稿をセット
		mav.addObject("comments", commentData);
		mav.setViewName("/show");
		return mav;
	}

	@GetMapping("date")
	public ModelAndView dateReport(@RequestParam(name = "startDate") String startDate,
			@RequestParam(name = "endDate") String endDate) throws ParseException {
		ModelAndView mav = new ModelAndView();
		List<Report> reportData = reportService.findByDateReport(startDate, endDate); // 投稿を全件取得
		mav.setViewName("/top"); // 画面遷移先を指定
		mav.addObject("startDate", startDate);
		mav.addObject("endDate", endDate);
		mav.addObject("reports", reportData); // 投稿データオブジェクトを保管
		return mav;
	}
}