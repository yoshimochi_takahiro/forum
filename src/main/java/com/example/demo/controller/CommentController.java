package com.example.demo.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;

	// 新規投稿画面
	@GetMapping("/comments/new/{id}")
	public ModelAndView newComment(@PathVariable Integer id) throws ParseException {
		ModelAndView mav = new ModelAndView(); // form用の空のentityの準備
		Comment comment = new Comment();
		comment.setReportId(id); // コメントをする投稿のIDをセット
		mav.setViewName("/comments/new"); // 画面遷移先を指定
		mav.addObject("formModel", comment); // 準備した空のentityを保管
		return mav;
	}

	// 投稿処理
	@PostMapping("/comments/add")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment) {
		commentService.saveComment(comment); // 投稿をテーブルに格納
		return new ModelAndView("redirect:/"); // rootへリダイレクト
	}

	// 投稿編集処理
	@GetMapping("/comments/edit/{id}")
	public ModelAndView editComment(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.editComment(id); // 編集する投稿を取得
		mav.addObject("formModel", comment); // 編集する投稿をセット
		mav.setViewName("/comments/edit");
		return mav;
	}

	// 投稿の編集内容更新処理
	@PutMapping("/comments/update/{id}")
	public ModelAndView updateComment(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment) {
		commentService.saveComment(comment); // 編集した投稿を更新
		return new ModelAndView("redirect:/");
	}

	// 投稿削除
	@DeleteMapping("/comments/delete/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id); // UrlParameterのidを基に投稿を削除
		return new ModelAndView("redirect:/");
	}
}
