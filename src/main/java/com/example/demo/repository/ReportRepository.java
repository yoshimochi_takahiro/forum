package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Report;

@Repository

public interface ReportRepository extends JpaRepository<Report, Integer> {
	List<Report> findByOrderByIdDesc();
	List<Report> findAllByOrderByUpdatedDateDescIdDesc();

	@Query(value = "SELECT * FROM Report WHERE created_date BETWEEN ?1 AND ?2 ORDER BY id Desc", nativeQuery = true)
	List<Report> findByDate(@Param("startDate") Date start, @Param("endDate") Date end);

	@Modifying
	@Transactional
	@Query("update Report u set updatedDate =  CURRENT_TIMESTAMP where id = :id")
	public int updateTimestamp(@Param("id") Integer id);
}